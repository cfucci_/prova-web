/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Date;


/**
 *
 * @author a1175548
 */
public class Login {

    /**
     * Creates a new instance of Login
     */
    private String login;
    private Date dataHora;
    
    public Login(String login, Date dataHora) {
        this.login = login;
        this.dataHora = dataHora;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Date getDataHora() {
        return dataHora;
    }

    public void setDataHora(Date dataHora) {
        this.dataHora = dataHora;
    }
    
    
    
}
