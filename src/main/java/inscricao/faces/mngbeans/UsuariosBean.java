/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inscricao.faces.mngbeans;

import entities.Login;
import java.io.Serializable;
import java.util.ArrayList;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author a1175548
 */
@Named(value = "usuariosBean")
@ApplicationScoped
public class UsuariosBean implements Serializable{
    
    private ArrayList<Login> list;

    /**
     * Creates a new instance of UsuariosBean
     */
    public UsuariosBean() {
        this.list = new ArrayList<Login>();
    }
    
    public void add(Login l){
        this.list.add(l);
    }

    public ArrayList<Login> getList() {
        return list;
    }

    public void setList(ArrayList<Login> list) {
        this.list = list;
    }
    
    
}
