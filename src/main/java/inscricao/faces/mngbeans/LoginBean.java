/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inscricao.faces.mngbeans;

import entities.Login;
import java.util.Date;
import javax.inject.Inject;
import javax.inject.Named;
import utfpr.faces.support.PageBean;

/**
 *
 * @author a1175548
 */
@Named(value = "loginBean")
@javax.enterprise.context.ApplicationScoped
public class LoginBean extends PageBean{
    
    private String usuario;
    private String senha;
    private boolean opadmin;
    private Date dataHora;
    private String resultado;
    @Inject
    private UsuariosBean user;
    
    /**
     * Creates a new instance of LoginBean
     */
    public LoginBean() {
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public boolean isOpadmin() {
        return opadmin;
    }

    public void setOpadmin(boolean opadmin) {
        this.opadmin = opadmin;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }
    
    public String acaoLogin(){
        //Login correto
        if(usuario.equals(senha)){
            this.setResultado("");
            this.dataHora = new Date();
            Login l = new Login(this.usuario, this.dataHora);
            UsuariosBean bean;
            bean = (UsuariosBean) getBean("usuariosBean");
            bean.add(l);
            if(this.opadmin){
                return "admin";
            }else{
                return "cadastro";
            }
        }
        this.resultado = "false";
        return null;
    }
    
}
